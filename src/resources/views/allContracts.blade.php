
@extends('layouts.app')

<script src="{{ asset('js/profile.js') }}" defer></script>

@section('content')

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>

<style>
  .row-table {
    transition: 0.25s ease;
    cursor: pointer;
  }


  .row-table:hover {
    transform: scale(1.01);
    background-color: #c5f2f9 !important;
  }

</style>

<div class="container-fluid bg-white">

  {{-- start name and email section --}}


	<div class="row bg-primary py-5">
		<div class="col-12">
			<h1 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">{{ \Auth::user()->name }}</h1>
    </div>
    <div class="col-12">
			<h3 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">All The Contracts</h3>
		</div>
		<div class="col-12">
			<div class=" mdc-typography--button  text-center text-white">

				{{ \Auth::user()->email }}
			</div>
		</div>
  </div>

  {{-- end name and email section --}}

	<div>
    {{-- {{ dd(Auth::user()->email) }} --}}
    {{-- @if ( Auth::user()->role->name == 'admin' ) --}}
      <main class="bg-white container-fluid py-5">

			<div class="row">

				<div class="col-md-12 col-sm-12 mx-auto">
					<div class="d-flex justify-content-between">
            <div class=" mt-1 mb-3  ">
              <a class="btn btn-primary rounded"  href="{{ route('profile.show', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                Back to your own Contracts
              </span>
              </a>
            </div>
            <div class=" mt-1 mb-3 ">
              <a class="btn btn-primary rounded" href="{{ route('quickInvoice.index', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                go to Quick invoices
              </span>
              </a>
            </div>
          </div>
          <div class="" id="demo">
            <div class="table-responsive-vertical shadow-z-1">
            <!-- Table starts here -->
              <table id="table" class="table table-hover table-mc-light-blue">
                <thead>
                  <tr>
                    <th class="font-weight-bold mdc-typography--body1">Contract No.</th>
                    <th class="font-weight-bold mdc-typography--body1">Client</th>
                    <th class="font-weight-bold mdc-typography--body1">Type</th>
                    <th class="font-weight-bold mdc-typography--body1">Amount in RMB</th>
                    <th class="font-weight-bold mdc-typography--body1">Comment</th>

                </thead>

                {{-- {{ dd(Auth::user()->role->name) }} --}}
                <tbody>
                  @forelse ($allContracts  as $contract)
                  <tr class='clickable-row row-table' data-href='{{ route('contract.show',[ 'user'=> Auth::id(), 'contract_id'=> $contract->id ]) }}'>
                    <td class="mdc-typography--body1">{{$contract->id}}</td>
                    <td class="mdc-typography--body1">{{$contract->Contact->name}}</td>

                    <td class="mdc-typography--body1">{{$contract->type}}</td>
                    <td class="mdc-typography--body1">{{$contract->contractAmount}}</td>
                    <td class="mdc-typography--body1">{{$contract->comment}}</td>

                  </tr>
                  @empty
                    <tr>
                      <td colspan="11" class="font-weight-bold mdc-typography--body1 text-center">no contracts</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>


					</div>
				</div>
			</div>
		</main>





  </div>


</div>


<div class="mdc-snackbar"  style="    z-index: 1000000000;">
  <div class="mdc-snackbar__surface">
    <div class="mdc-snackbar__label"	role="status" aria-live="polite"></div>
    <div class="mdc-snackbar__actions">
      <button type="button" class="mdc-button mdc-snackbar__action">ok</button>
    </div>
  </div>
</div>

@if($errors->any())
  <div class="mdc-snackbar mdc-snackbar-skill"  style="    z-index: 1000000000;">
    <div class="mdc-snackbar__surface">
      <div class="mdc-snackbar__label" role="status" aria-live="polite">
        {{$errors->first()}}
      </div>
      <div class="mdc-snackbar__actions">
        <button type="button" class="mdc-button mdc-snackbar__action">ok</button>
      </div>
    </div>
  </div>
@endif

<script type="text/javascript">
// jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#skill').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('skills.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'skill':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)
          $('#skill_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#skill_list li', function(){
      // declare the value in the input field to a variable
      var value = $(this).text();
      // assign the value to the search box
      $('#skill').val(value);
      // after click is done, search results segment is made empty
      $('#skill_list').html("");
    });
  });
</script>


<script type="text/javascript">
  // jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#contact').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('contacts.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'contact':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)

          $('#contact_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#contact_list li', function(){

      // declare the value in the input field to a variable
      var value = $(this).text();

      // assign the value to the search box
      $('#contact').val(value.split(' ')[0]);
      // after click is done, search results segment is made empty

      $('#contact_list').html("");
      $('#contactIdInput').html('	<input name="contactId" id=contactId type="hidden" value="'+$(this).children().text()+'" >');

    });
  });
</script>

@endsection
