
@extends('layouts.app')

<script src="{{ asset('js/profile.js') }}" defer></script>

@section('content')

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>

<style>
  .row-table {
    transition: 0.25s ease;
    cursor: pointer;
  }


  .row-table:hover {
    transform: scale(1.01);
    background-color: #c5f2f9 !important;
  }

</style>

<div class="container-fluid bg-white">

  {{-- start name and email section --}}


	<div class="row bg-primary py-5">
		<div class="col-12">
			<h1 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">{{ \Auth::user()->name }}</h1>
    </div>
    <div class="col-12">
			<h3 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">Contracts</h3>
		</div>
		<div class="col-12">
			<div class=" mdc-typography--button  text-center text-white">

				{{ \Auth::user()->email }}
      </div>

		</div>
  </div>

  {{-- end name and email section --}}

	<div>
    {{-- {{ dd(Auth::user()->email) }} --}}
    {{-- @if ( Auth::user()->role->name == 'admin' ) --}}
      <main class="bg-white container-fluid py-5">

			<div class="row">

				<div class="col-md-12 col-sm-12 mx-auto">
					<div class="d-flex justify-content-between">
            <div class=" mt-1 mb-3  ">
              <a class="" href="{{ route('profile.show', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                Contracts
              </span>
              </a>
            </div>

            @if (\Auth::user()->role->name == 'admin')
              <div class=" mt-1 mb-3 ">
                <a class="btn btn-primary rounded"  href="/profile">
                  <span class=" mdc-typography--button">
                    View All contracts
                  </span>
                </a>
            </div>


            @endif

            <div class=" mt-1 mb-3 ">
              <a class="btn btn-primary rounded" href="{{ route('quickInvoice.index', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                go to Quick invoices
              </span>
              </a>
            </div>
          </div>
          <div class="" id="demo">
            <div class="table-responsive-vertical shadow-z-1">
            <!-- Table starts here -->
              <table id="table" class="table table-hover table-mc-light-blue">
                <thead>
                  <tr>
                    <th class="font-weight-bold mdc-typography--body1">Contract No.</th>
                    <th class="font-weight-bold mdc-typography--body1">Client</th>
                    <th class="font-weight-bold mdc-typography--body1">Type</th>
                    <th class="font-weight-bold mdc-typography--body1">Amount in RMB</th>
                    <th class="font-weight-bold mdc-typography--body1">Comment</th>

                </thead>

                {{-- {{ dd(Auth::user()->role->name) }} --}}
                <tbody>
                  @forelse ($contracts  as $contract)
                  <tr class='clickable-row row-table' data-href='{{ route('contract.show',[ 'user'=> Auth::id(), 'contract_id'=> $contract->id ]) }}'>
                    <td class="mdc-typography--body1">{{$contract->id}}</td>
                    <td class="mdc-typography--body1">{{$contract->Contact->name}}</td>

                    <td class="mdc-typography--body1">{{$contract->type}}</td>
                    <td class="mdc-typography--body1">{{$contract->contractAmount}}</td>
                    <td class="mdc-typography--body1">{{$contract->comment}}</td>

                  </tr>
                  @empty
                    <tr>
                      <td colspan="11" class="font-weight-bold mdc-typography--body1 text-center">no contracts</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>

            <div class="row justfy-content-end">
						<div class="col" align=right>
							<button class="mdc-fab mr-5 my-fab " aria-label="Favorite"  data-toggle="modal" data-target="#ContractModal" style="margin-top:-30px;">
								<span class="mdc-fab__icon material-icons">add</span>
							</button>
						</div>
					</div>
					</div>
				</div>
			</div>
		</main>
    {{-- @else --}}
      {{-- <h1>you are not an admin</h1> --}}
    {{-- @endif --}}
    {{-- start contract display section --}}

    {{-- end contract display section --}}

		<!--start Modal contract -->
		<div class="modal fade " id="ContractModal" tabindex="-100" role="dialog" aria-labelledby="contractModal" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg " role="document">
				<div class="modal-content home-card">
					<div class="modal-header" style="border:none;">
						<h5 class="modal-title mdc-typography--button" id="ContractModalLabel">add a new contract</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" >

						<form id="contractForm" action="{{ route('contract.add')}}" method="POST" name="contract" >
							@csrf
							@method('PUT')
							<div id="contactIdInput" ></div>
							<div class="row mb-4">

								<div class="col" >
									<div class="text-center toolbar mdc-menu-surface--anchor">
										<div style="width: 80%;" class="mdc-text-field mdc-text-field--outlined contactTextField  ">
											<input  type="text" id="contact" class="mdc-text-field__input" autocomplete="off">
											<div class="mdc-notched-outline">
												<div class="mdc-notched-outline__leading"></div>
												<div class="mdc-notched-outline__notch">
													<label for="tf-outlined" class="mdc-floating-label">Add an existing contact</label>
												</div>
												<div class="mdc-notched-outline__trailing"></div>
											</div>
										</div>
										<div class="mdc-menu mdc-menu-surface mdcContactsList" style="margin-top:60px;">
											<ul class="mdc-list" role="menu" id="contact_list" aria-hidden="true" aria-orientation="vertical" tabindex="-1">
											</ul>
										</div>
									</div>
								</div>

							</div>
							<div class="row ">

								<div class="col-md-12 col-sm-12" >

									<div class="row justfy-content-center" >
										<div class="col-12 mb-3" >
											<div class="mdc-typography--button" class="font-weight-light ">Contact info </div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="contactName" class="mdc-text-field mdc-text-field--outlined">
												<input  class="mdc-text-field__input form-control"  name="contactName" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">ContactName</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>


												</div>
											</div>
										</div>
									</div>

									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;"  id="contactFonction"  class="mdc-text-field mdc-text-field--outlined">
												<input  class="mdc-text-field__input form-control"  name="fonction" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Fonction</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
                    <div style="    display: inherit; margin: auto;">
                      <div class="col text-left" >
                        <div class="mdc-select mdc-select--outlined" id=phone style=" width: 200px;" >
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">
                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
                          <input type="hidden" name="phone" value=86 style=" width: 150px;">
                          <i class="mdc-select__dropdown-icon"></i>
                          <div class="mdc-select__selected-text" ></div>
                          <div class="mdc-select__menu mdc-menu mdc-menu-surface " style="z-index:10000" >
                            @include('layouts.phone')
                          </div>
                        </div>
                      </div>


                        <div class="col " >
                          <div style="width: 380px;" id="phone_number" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
                            <input class="mdc-text-field__input form-control"  name="phoneNumber" required>
                            <div class="mdc-notched-outline">
                              <div class="mdc-notched-outline__leading"></div>
                              <div class="mdc-notched-outline__notch">
                                <label for="tf-outlined" class="mdc-floating-label">Phone number</label>
                              </div>
                              <div class="mdc-notched-outline__trailing"></div>
                            </div>
                          </div>
                        </div>
                    </div>

									</div>


								</div>
								<div class="col-md-12 col-sm-12">
									<div class="row justfy-content-center" >
										<div class="col-12 mb-3" >
											<div class="mdc-typography--button" class="font-weight-light ">Contact address </div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="adressName" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="name" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Address name</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="adressNumber"  class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="number" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Address number</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>

									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="adressCity"  class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="city" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label"> City</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
                    <div style="    display: inherit; margin: auto;">
                      <div class="col" >
                        <div style="width: 200px;" class="mdc-select mdc-select--outlined container-fluid" id="phone"  >
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">
                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
                          <input type="hidden" value="China" name="country" required>
                          <i class="mdc-select__dropdown-icon"></i>
                          <div class="mdc-select__selected-text" ></div>
                          <div class="mdc-select__menu mdc-menu mdc-menu-surface " style="z-index:10000" >
                            <ul class="mdc-list">
                              <li class="mdc-list-item"value="Afghanistan">Afghanistan	</li>
                              <li class="mdc-list-item"value="Åland Islands">Åland Islands	</li>
                              <li class="mdc-list-item"value="Albania">Albania	</li>
                              <li class="mdc-list-item"value="Algeria">Algeria	</li>
                              <li class="mdc-list-item"value="American Samoa">American Samoa	</li>
                              <li class="mdc-list-item"value="Andorra">Andorra	</li>
                              <li class="mdc-list-item"value="Angola">Angola	</li>
                              <li class="mdc-list-item"value="Anguilla">Anguilla	</li>
                              <li class="mdc-list-item"value="Antarctica">Antarctica	</li>
                              <li class="mdc-list-item"value="Antigua and Barbuda">Antigua and Barbuda	</li>
                              <li class="mdc-list-item"value="Argentina">Argentina	</li>
                              <li class="mdc-list-item"value="Armenia">Armenia	</li>
                              <li class="mdc-list-item"value="Aruba">Aruba	</li>
                              <li class="mdc-list-item"value="Australia">Australia	</li>
                              <li class="mdc-list-item"value="Austria">Austria	</li>
                              <li class="mdc-list-item"value="Azerbaijan">Azerbaijan	</li>
                              <li class="mdc-list-item"value="Bahamas">Bahamas	</li>
                              <li class="mdc-list-item"value="Bahrain">Bahrain	</li>
                              <li class="mdc-list-item"value="Bangladesh">Bangladesh	</li>
                              <li class="mdc-list-item"value="Barbados">Barbados	</li>
                              <li class="mdc-list-item"value="Belarus">Belarus	</li>
                              <li class="mdc-list-item"value="Belgium">Belgium	</li>
                              <li class="mdc-list-item"value="Belize">Belize	</li>
                              <li class="mdc-list-item"value="Benin">Benin	</li>
                              <li class="mdc-list-item"value="Bermuda">Bermuda	</li>
                              <li class="mdc-list-item"value="Bhutan">Bhutan	</li>
                              <li class="mdc-list-item"value="Bolivia">Bolivia	</li>
                              <li class="mdc-list-item"value="Bosnia and Herzegovina">Bosnia and Herzegovina	</li>
                              <li class="mdc-list-item"value="Botswana">Botswana	</li>
                              <li class="mdc-list-item"value="Bouvet Island">Bouvet Island	</li>
                              <li class="mdc-list-item"value="Brazil">Brazil	</li>
                              <li class="mdc-list-item"value="British Indian Ocean Territory">British Indian Ocean Territory	</li>
                              <li class="mdc-list-item"value="Brunei Darussalam">Brunei Darussalam	</li>
                              <li class="mdc-list-item"value="Bulgaria">Bulgaria	</li>
                              <li class="mdc-list-item"value="Burkina Faso">Burkina Faso	</li>
                              <li class="mdc-list-item"value="Burundi">Burundi	</li>
                              <li class="mdc-list-item"value="Cambodia">Cambodia	</li>
                              <li class="mdc-list-item"value="Cameroon">Cameroon	</li>
                              <li class="mdc-list-item"value="Canada">Canada	</li>
                              <li class="mdc-list-item"value="Cape Verde">Cape Verde	</li>
                              <li class="mdc-list-item"value="Cayman Islands">Cayman Islands	</li>
                              <li class="mdc-list-item"value="Central African Republic">Central African Republic	</li>
                              <li class="mdc-list-item"value="Chad">Chad	</li>
                              <li class="mdc-list-item"value="Chile">Chile	</li>
                              <li class="mdc-list-item"value="China" selected>China	</li>
                              <li class="mdc-list-item"value="Christmas Island">Christmas Island	</li>
                              <li class="mdc-list-item"value="Cocos (Keeling) Islands">Cocos (Keeling) Islands	</li>
                              <li class="mdc-list-item"value="Colombia">Colombia	</li>
                              <li class="mdc-list-item"value="Comoros">Comoros	</li>
                              <li class="mdc-list-item"value="Congo">Congo	</li>
                              <li class="mdc-list-item"value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The	</li>
                              <li class="mdc-list-item"value="Cook Islands">Cook Islands	</li>
                              <li class="mdc-list-item"value="Costa Rica">Costa Rica	</li>
                              <li class="mdc-list-item"value="Cote D'ivoire">Cote D'ivoire	</li>
                              <li class="mdc-list-item"value="Croatia">Croatia	</li>
                              <li class="mdc-list-item"value="Cuba">Cuba	</li>
                              <li class="mdc-list-item"value="Cyprus">Cyprus	</li>
                              <li class="mdc-list-item"value="Czech Republic">Czech Republic	</li>
                              <li class="mdc-list-item"value="Denmark">Denmark	</li>
                              <li class="mdc-list-item"value="Djibouti">Djibouti	</li>
                              <li class="mdc-list-item"value="Dominica">Dominica	</li>
                              <li class="mdc-list-item"value="Dominican Republic">Dominican Republic	</li>
                              <li class="mdc-list-item"value="Ecuador">Ecuador	</li>
                              <li class="mdc-list-item"value="Egypt">Egypt	</li>
                              <li class="mdc-list-item"value="El Salvador">El Salvador	</li>
                              <li class="mdc-list-item"value="Equatorial Guinea">Equatorial Guinea	</li>
                              <li class="mdc-list-item"value="Eritrea">Eritrea	</li>
                              <li class="mdc-list-item"value="Estonia">Estonia	</li>
                              <li class="mdc-list-item"value="Ethiopia">Ethiopia	</li>
                              <li class="mdc-list-item"value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)	</li>
                              <li class="mdc-list-item"value="Faroe Islands">Faroe Islands	</li>
                              <li class="mdc-list-item"value="Fiji">Fiji	</li>
                              <li class="mdc-list-item"value="Finland">Finland	</li>
                              <li class="mdc-list-item"value="France">France	</li>
                              <li class="mdc-list-item"value="French Guiana">French Guiana	</li>
                              <li class="mdc-list-item"value="French Polynesia">French Polynesia	</li>
                              <li class="mdc-list-item"value="French Southern Territories">French Southern Territories	</li>
                              <li class="mdc-list-item"value="Gabon">Gabon	</li>
                              <li class="mdc-list-item"value="Gambia">Gambia	</li>
                              <li class="mdc-list-item"value="Georgia">Georgia	</li>
                              <li class="mdc-list-item"value="Germany">Germany	</li>
                              <li class="mdc-list-item"value="Ghana">Ghana	</li>
                              <li class="mdc-list-item"value="Gibraltar">Gibraltar	</li>
                              <li class="mdc-list-item"value="Greece">Greece	</li>
                              <li class="mdc-list-item"value="Greenland">Greenland	</li>
                              <li class="mdc-list-item"value="Grenada">Grenada	</li>
                              <li class="mdc-list-item"value="Guadeloupe">Guadeloupe	</li>
                              <li class="mdc-list-item"value="Guam">Guam	</li>
                              <li class="mdc-list-item"value="Guatemala">Guatemala	</li>
                              <li class="mdc-list-item"value="Guernsey">Guernsey	</li>
                              <li class="mdc-list-item"value="Guinea">Guinea	</li>
                              <li class="mdc-list-item"value="Guinea-bissau">Guinea-bissau	</li>
                              <li class="mdc-list-item"value="Guyana">Guyana	</li>
                              <li class="mdc-list-item"value="Haiti">Haiti	</li>
                              <li class="mdc-list-item"value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands	</li>
                              <li class="mdc-list-item"value="Holy See (Vatican City State)">Holy See (Vatican City State)	</li>
                              <li class="mdc-list-item"value="Honduras">Honduras	</li>
                              <li class="mdc-list-item"value="Hong Kong">Hong Kong	</li>
                              <li class="mdc-list-item"value="Hungary">Hungary	</li>
                              <li class="mdc-list-item"value="Iceland">Iceland	</li>
                              <li class="mdc-list-item"value="India">India	</li>
                              <li class="mdc-list-item"value="Indonesia">Indonesia	</li>
                              <li class="mdc-list-item"value="Iran, Islamic Republic of">Iran, Islamic Republic of	</li>
                              <li class="mdc-list-item"value="Iraq">Iraq	</li>
                              <li class="mdc-list-item"value="Ireland">Ireland	</li>
                              <li class="mdc-list-item"value="Isle of Man">Isle of Man	</li>
                              <li class="mdc-list-item"value="Israel">Israel	</li>
                              <li class="mdc-list-item"value="Italy">Italy	</li>
                              <li class="mdc-list-item"value="Jamaica">Jamaica	</li>
                              <li class="mdc-list-item"value="Japan">Japan	</li>
                              <li class="mdc-list-item"value="Jersey">Jersey	</li>
                              <li class="mdc-list-item"value="Jordan">Jordan	</li>
                              <li class="mdc-list-item"value="Kazakhstan">Kazakhstan	</li>
                              <li class="mdc-list-item"value="Kenya">Kenya	</li>
                              <li class="mdc-list-item"value="Kiribati">Kiribati	</li>
                              <li class="mdc-list-item"value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of	</li>
                              <li class="mdc-list-item"value="Korea, Republic of">Korea, Republic of	</li>
                              <li class="mdc-list-item"value="Kuwait">Kuwait	</li>
                              <li class="mdc-list-item"value="Kyrgyzstan">Kyrgyzstan	</li>
                              <li class="mdc-list-item"value="Lao People's Democratic Republic">Lao People's Democratic Republic	</li>
                              <li class="mdc-list-item"value="Latvia">Latvia	</li>
                              <li class="mdc-list-item"value="Lebanon">Lebanon	</li>
                              <li class="mdc-list-item"value="Lesotho">Lesotho	</li>
                              <li class="mdc-list-item"value="Liberia">Liberia	</li>
                              <li class="mdc-list-item"value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya	</li>
                              <li class="mdc-list-item"value="Liechtenstein">Liechtenstein	</li>
                              <li class="mdc-list-item"value="Lithuania">Lithuania	</li>
                              <li class="mdc-list-item"value="Luxembourg">Luxembourg	</li>
                              <li class="mdc-list-item"value="Macao">Macao	</li>
                              <li class="mdc-list-item"value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of	</li>
                              <li class="mdc-list-item"value="Madagascar">Madagascar	</li>
                              <li class="mdc-list-item"value="Malawi">Malawi	</li>
                              <li class="mdc-list-item"value="Malaysia">Malaysia	</li>
                              <li class="mdc-list-item"value="Maldives">Maldives	</li>
                              <li class="mdc-list-item"value="Mali">Mali	</li>
                              <li class="mdc-list-item"value="Malta">Malta	</li>
                              <li class="mdc-list-item"value="Marshall Islands">Marshall Islands	</li>
                              <li class="mdc-list-item"value="Martinique">Martinique	</li>
                              <li class="mdc-list-item"value="Mauritania">Mauritania	</li>
                              <li class="mdc-list-item"value="Mauritius">Mauritius	</li>
                              <li class="mdc-list-item"value="Mayotte">Mayotte	</li>
                              <li class="mdc-list-item"value="Mexico">Mexico	</li>
                              <li class="mdc-list-item"value="Micronesia, Federated States of">Micronesia, Federated States of	</li>
                              <li class="mdc-list-item"value="Moldova, Republic of">Moldova, Republic of	</li>
                              <li class="mdc-list-item"value="Monaco">Monaco	</li>
                              <li class="mdc-list-item"value="Mongolia">Mongolia	</li>
                              <li class="mdc-list-item"value="Montenegro">Montenegro	</li>
                              <li class="mdc-list-item"value="Montserrat">Montserrat	</li>
                              <li class="mdc-list-item"value="Morocco">Morocco	</li>
                              <li class="mdc-list-item"value="Mozambique">Mozambique	</li>
                              <li class="mdc-list-item"value="Myanmar">Myanmar	</li>
                              <li class="mdc-list-item"value="Namibia">Namibia	</li>
                              <li class="mdc-list-item"value="Nauru">Nauru	</li>
                              <li class="mdc-list-item"value="Nepal">Nepal	</li>
                              <li class="mdc-list-item"value="Netherlands">Netherlands	</li>
                              <li class="mdc-list-item"value="Netherlands Antilles">Netherlands Antilles	</li>
                              <li class="mdc-list-item"value="New Caledonia">New Caledonia	</li>
                              <li class="mdc-list-item"value="New Zealand">New Zealand	</li>
                              <li class="mdc-list-item"value="Nicaragua">Nicaragua	</li>
                              <li class="mdc-list-item"value="Niger">Niger	</li>
                              <li class="mdc-list-item"value="Nigeria">Nigeria	</li>
                              <li class="mdc-list-item"value="Niue">Niue	</li>
                              <li class="mdc-list-item"value="Norfolk Island">Norfolk Island	</li>
                              <li class="mdc-list-item"value="Northern Mariana Islands">Northern Mariana Islands	</li>
                              <li class="mdc-list-item"value="Norway">Norway	</li>
                              <li class="mdc-list-item"value="Oman">Oman	</li>
                              <li class="mdc-list-item"value="Pakistan">Pakistan	</li>
                              <li class="mdc-list-item"value="Palau">Palau	</li>
                              <li class="mdc-list-item"value="Palestinian Territory, Occupied">Palestinian Territory, Occupied	</li>
                              <li class="mdc-list-item"value="Panama">Panama	</li>
                              <li class="mdc-list-item"value="Papua New Guinea">Papua New Guinea	</li>
                              <li class="mdc-list-item"value="Paraguay">Paraguay	</li>
                              <li class="mdc-list-item"value="Peru">Peru	</li>
                              <li class="mdc-list-item"value="Philippines">Philippines	</li>
                              <li class="mdc-list-item"value="Pitcairn">Pitcairn	</li>
                              <li class="mdc-list-item"value="Poland">Poland	</li>
                              <li class="mdc-list-item"value="Portugal">Portugal	</li>
                              <li class="mdc-list-item"value="Puerto Rico">Puerto Rico	</li>
                              <li class="mdc-list-item"value="Qatar">Qatar	</li>
                              <li class="mdc-list-item"value="Reunion">Reunion	</li>
                              <li class="mdc-list-item"value="Romania">Romania	</li>
                              <li class="mdc-list-item"value="Russian Federation">Russian Federation	</li>
                              <li class="mdc-list-item"value="Rwanda">Rwanda	</li>
                              <li class="mdc-list-item"value="Saint Helena">Saint Helena	</li>
                              <li class="mdc-list-item"value="Saint Kitts and Nevis">Saint Kitts and Nevis	</li>
                              <li class="mdc-list-item"value="Saint Lucia">Saint Lucia	</li>
                              <li class="mdc-list-item"value="Saint Pierre and Miquelon">Saint Pierre and Miquelon	</li>
                              <li class="mdc-list-item"value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines	</li>
                              <li class="mdc-list-item"value="Samoa">Samoa	</li>
                              <li class="mdc-list-item"value="San Marino">San Marino	</li>
                              <li class="mdc-list-item"value="Sao Tome and Principe">Sao Tome and Principe	</li>
                              <li class="mdc-list-item"value="Saudi Arabia">Saudi Arabia	</li>
                              <li class="mdc-list-item"value="Senegal">Senegal	</li>
                              <li class="mdc-list-item"value="Serbia">Serbia	</li>
                              <li class="mdc-list-item"value="Seychelles">Seychelles	</li>
                              <li class="mdc-list-item"value="Sierra Leone">Sierra Leone	</li>
                              <li class="mdc-list-item"value="Singapore">Singapore	</li>
                              <li class="mdc-list-item"value="Slovakia">Slovakia	</li>
                              <li class="mdc-list-item"value="Slovenia">Slovenia	</li>
                              <li class="mdc-list-item"value="Solomon Islands">Solomon Islands	</li>
                              <li class="mdc-list-item"value="Somalia">Somalia	</li>
                              <li class="mdc-list-item"value="South Africa">South Africa	</li>
                              <li class="mdc-list-item"value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands	</li>
                              <li class="mdc-list-item"value="Spain">Spain	</li>
                              <li class="mdc-list-item"value="Sri Lanka">Sri Lanka	</li>
                              <li class="mdc-list-item"value="Sudan">Sudan	</li>
                              <li class="mdc-list-item"value="Suriname">Suriname	</li>
                              <li class="mdc-list-item"value="Svalbard and Jan Mayen">Svalbard and Jan Mayen	</li>
                              <li class="mdc-list-item"value="Swaziland">Swaziland	</li>
                              <li class="mdc-list-item"value="Sweden">Sweden	</li>
                              <li class="mdc-list-item"value="Switzerland">Switzerland	</li>
                              <li class="mdc-list-item"value="Syrian Arab Republic">Syrian Arab Republic	</li>
                              <li class="mdc-list-item"value="Taiwan, Province of China">Taiwan, Province of China	</li>
                              <li class="mdc-list-item"value="Tajikistan">Tajikistan	</li>
                              <li class="mdc-list-item"value="Tanzania, United Republic of">Tanzania, United Republic of	</li>
                              <li class="mdc-list-item"value="Thailand">Thailand	</li>
                              <li class="mdc-list-item"value="Timor-leste">Timor-leste	</li>
                              <li class="mdc-list-item"value="Togo">Togo	</li>
                              <li class="mdc-list-item"value="Tokelau">Tokelau	</li>
                              <li class="mdc-list-item"value="Tonga">Tonga	</li>
                              <li class="mdc-list-item"value="Trinidad and Tobago">Trinidad and Tobago	</li>
                              <li class="mdc-list-item"value="Tunisia">Tunisia	</li>
                              <li class="mdc-list-item"value="Turkey">Turkey	</li>
                              <li class="mdc-list-item"value="Turkmenistan">Turkmenistan	</li>
                              <li class="mdc-list-item"value="Turks and Caicos Islands">Turks and Caicos Islands	</li>
                              <li class="mdc-list-item"value="Tuvalu">Tuvalu	</li>
                              <li class="mdc-list-item"value="Uganda">Uganda	</li>
                              <li class="mdc-list-item"value="Ukraine">Ukraine	</li>
                              <li class="mdc-list-item"value="United Arab Emirates">United Arab Emirates	</li>
                              <li class="mdc-list-item"value="United Kingdom">United Kingdom	</li>
                              <li class="mdc-list-item"value="United States">United States	</li>
                              <li class="mdc-list-item"value="United States Minor Outlying Islands">United States Minor Outlying Islands	</li>
                              <li class="mdc-list-item"value="Uruguay">Uruguay	</li>
                              <li class="mdc-list-item"value="Uzbekistan">Uzbekistan	</li>
                              <li class="mdc-list-item"value="Vanuatu">Vanuatu	</li>
                              <li class="mdc-list-item"value="Venezuela">Venezuela	</li>
                              <li class="mdc-list-item"value="Viet Nam">Viet Nam	</li>
                              <li class="mdc-list-item"value="Virgin Islands, British">Virgin Islands, British	</li>
                              <li class="mdc-list-item"value="Virgin Islands, U.S.">Virgin Islands, U.S.	</li>
                              <li class="mdc-list-item"value="Wallis and Futuna">Wallis and Futuna	</li>
                              <li class="mdc-list-item"value="Western Sahara">Western Sahara	</li>
                              <li class="mdc-list-item"value="Yemen">Yemen	</li>
                              <li class="mdc-list-item"value="Zambia">Zambia	</li>
                              <li class="mdc-list-item"value="Zimbabwe">Zimbabwe	</li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div class="col" >
                        <div style="width: 381px;" id="adressZip" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
                          <input class="mdc-text-field__input form-control"  name="zip_code" required>
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">
                              <label for="tf-outlined" class="mdc-floating-label"> Zip code</label>
                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
                        </div>
                      </div>
                    </div>
									</div>
									{{-- <div class="row mb-4"> --}}
									{{-- </div> --}}
								</div>
								<div class="col-md-12 col-sm-12" >
									<div class="row justfy-content-center" >
										<div class="col-12 mb-3" >
											<div class="mdc-typography--button" class="font-weight-light ">Contact's comany infos </div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="englishName" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="english_name" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label"> English name</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>

												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div  class="col text-center" >
											<div style="width: 80%;" id="chineseName" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="chinese_name" >
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label"> Chinese name</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>

												</div>
											</div>
										</div>
									</div>

									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="companyCountry" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="company_country" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label"> company country</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="companyPhone" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="company_phone" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label"> Company phone</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="taxNumber" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="tax_number" >
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Tax number</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-sm-12" >

									<div class="row justfy-content-center" >
										<div class="col-12 mb-3" >
											<div class="mdc-typography--button" class="font-weight-light ">Contact bank infos </div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="contactBankAccount" class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="account_name" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Bank name</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>

												</div>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col text-center" >
											<div style="width: 80%;" id="contactBankAccountNumber"  class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="account_number" required>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Bank account number</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>

												</div>
											</div>
										</div>
									</div>

									<div class="row mb-4 justfy-content-start">
										<div class="col text-center" >
											<div style="width: 80%;" id="contactBankBranch"  class="mdc-text-field mdc-text-field--outlined  mdc-text-field--disabled">
												<input class="mdc-text-field__input form-control"  name="bank_branch">
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Bank branch</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
										</div>
                  </div>

								</div>
							</div>
							<div class="row justify-content-start">
								<div class="col-md-12 col-sm-12">
									<div class="row" >
										<div class="col-12 mb-3" >
											<div class="mdc-typography--button" class="font-weight-light ">Contract infos </div>
										</div>
									</div>
									<div class="row">
										<input   class="mdc-text-field__input form-control"  name="state" value="DRAFT" type="hidden" >
									</div>
									<div class="row mb-4">
                    <div style="    display: inherit; margin: auto;">

                      <div class="col" >
                        <div style="width: 140px;" class="mdc-select mdc-select--outlined container-fluid" id=contractType >
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">

                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
                          <input type="hidden" name="type" value="daily rate">
                          <i class="mdc-select__dropdown-icon"></i>
                          <div class="mdc-select__selected-text" >Package</div>
                          <div class="mdc-select__menu mdc-menu mdc-menu-surface " style="z-index:10000" >
                            <ul class="mdc-list">
                              <li class="mdc-list-item" data-value="daily rate" >
                                daily rate
                              </li>
                              <li class="mdc-list-item" data-value="package">
                                package
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div class="col ">
                        <div style="width: 437px;" class="mdc-text-field mdc-text-field--outlined">
                          <input   class="mdc-text-field__input form-control"  name="amount" >
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">
                              <label for="tf-outlined" class="mdc-floating-label">Amount in RMB</label>
                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
											</div>
                      </div>
                    </div>
									</div>
									{{-- <div class="row mb-4"> --}}
									{{-- </div> --}}
									<div class="row mb-4">
										<div class="col text-center">
                      <div style="width: 80%;" class="mdc-text-field mdc-text-field--outlined">
                          <input   class="mdc-text-field__input form-control"  name="comment" >
                          <div class="mdc-notched-outline">
                            <div class="mdc-notched-outline__leading"></div>
                            <div class="mdc-notched-outline__notch">
                              <label for="tf-outlined" class="mdc-floating-label">Comment</label>
                            </div>
                            <div class="mdc-notched-outline__trailing"></div>
                          </div>
                        </div>
										</div>
									</div>
								</div>
							</div>



							<div class="col-12" align=center>
								<button  type=submit class="shadow-none mdc-button mdc-button--unelevated  mdc-button--outlined my-custom-button" >
									<span class="mdc-button__label ">Create a new contract</span>
								</button>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>


    <!--end Modal contract -->


		<div class="modal fade " id="skillsModal" tabindex="-1" role="dialog" aria-labelledby="skillsModal" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered " role="document">
				<div class="modal-content my-img">
					<div class="modal-header mdc-typography--button" style="border:none;">
						<div class="col-12 pt-2" >
							add skills
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="material-icons mdc-button__icon">close</i>
							</button>
						</div>
					</div>

					<div class="modal-body" >
						<div class="container" >
							<div class="row justfy-content-center" >
								<div class="col-12 " align=center>
									<form class=" py-auto">
										<div class="toolbar mdc-menu-surface--anchor">
											<div class="mdc-text-field mdc-text-field--outlined skillTextField  mdc-text-field--with-trailing-icon">
												<input type="text" id="skill" class="mdc-text-field__input" autocomplete="off">
												<i class="material-icons mdc-text-field__icon" id="addChips" tabindex="0" role="button">add_circle</i>
												<div class="mdc-notched-outline">
													<div class="mdc-notched-outline__leading"></div>
													<div class="mdc-notched-outline__notch">
														<label for="tf-outlined" class="mdc-floating-label">Add a skill</label>
													</div>
													<div class="mdc-notched-outline__trailing"></div>
												</div>
											</div>
											<div class="mdc-menu mdc-menu-surface mdcSkillsList" style="margin-top:60px">
												<ul class="mdc-list" role="menu" id="skill_list" aria-hidden="true" aria-orientation="vertical" tabindex="-1">
												</ul>
											</div>
										</div>
									</form>
								</div>
              </div>

							<form action="{{route('skills.add')}}" method="GET" name="addSkills" id="chips"></form>
              <div class="col-12 " align=center>                                                                                                                                                                                   <button type=submit class="shadow-none mdc-button mdc-button--unelevated  mdc-button--outlined my-custom-button"  id="validate" >                                                                                     <span class="mdc-button__label ">save</span>                                                                                                                                                          </button>                                                                                                                                                                                                                                                                                                                                                                                                           </div>
							  <hr>
							  <div class="row justfy-content-center" >
								  <div class="col-12 mb-3" >
									  <div class="mdc-typography--button" class="font-weight-light ">Delete a skill </div>
								  </div>
							  </div>
							  <div class="row mx-1 justfy-content-start" >

								@forelse ($hardSkills as $skill)

								<form action="{{ route('skill.destroy')}}" method="post" type=hidden name="form{{$skill->id}}">
									@csrf
									@method('DELETE')
									<input name="skill_id" value="{{$skill->id}}" type="hidden"/>
									<div class="mdc-chip mr-2 " onClick="document.forms['form{{$skill->id}}'].submit();">
										<i class="material-icons mdc-chip__icon mdc-chip__icon--leading">remove_circle</i>
										<div class="mdc-chip__text">	{{ $skill->name }}</div>
									</div>
								</form>
								@empty
								<p>No hard skills yet</p>

								@endforelse

							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="modal fade " id="infosModal" tabindex="-1" role="dialog" aria-labelledby="skillsModal" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered " role="document">
				<div class="modal-content my-img">
					<div class="modal-header pb-0" style="border:none;">
            <div class="col-12 pt-2 mdc-typography--button" >
              Edit my profile
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="material-icons mdc-button__icon">close</i>
              </button>
            </div>
					</div>

					<div class="modal-body" >
						<div class="container" >
							<div class="row justfy-content-center" >
								<div class="col-12 text-center text-md-left">

									<form action="{{ route('profile.edit')}}" method="post" name="wechat" class="">
										@csrf
										@method('PUT')

										<div class="mdc-text-field mdc-text-field--outlined  mdc-text-field--with-trailing-icon">
											<input type="text"  name="wechat" class="mdc-text-field__input" autocomplete="off" required>
											<button class="material-icons mdc-text-field__icon"  tabindex="0"  type="submit" >edit</button>
											<div class="mdc-notched-outline">
												<div class="mdc-notched-outline__leading"></div>
												<div class="mdc-notched-outline__notch">
													<label for="tf-outlined" class="mdc-floating-label">Wechat id</label>
												</div>
												<div class="mdc-notched-outline__trailing"></div>
											</div>
										</div>

										<button type=submit class="shadow-none mdc-button mdc-button--unelevated  mdc-button--outlined my-custom-button ml-0 ml-md-5 mt-2 mt-md-0 "  >
											<span class="mdc-button__label ">save</span>
										</button>
									</form>

								</div>

							</div>
							<hr>

							<form action="{{ route('profile.edit')}}" method="post" class="row ">
								@csrf
								@method('PUT')
								<div class="col-md-12 col-sm-12 pt-3" align="left">
									<div class="mdc-select mdc-select--outlined container-fluid" id=phone >
										<div class="mdc-notched-outline">
											<div class="mdc-notched-outline__leading"></div>
											<div class="mdc-notched-outline__notch"></div>
											<div class="mdc-notched-outline__trailing"></div>
										</div>
										<input type="hidden" name="phone" value=86>
										<i class="mdc-select__dropdown-icon"></i>
										<div class="mdc-select__selected-text" ></div>
										<div class="mdc-select__menu mdc-menu mdc-menu-surface " style="z-index:10000" >
											@include('layouts.phone')
										</div>
									</div>
									<div class="mdc-text-field mdc-text-field--outlined  mdc-text-field--with-trailing-icon mt-2">
										<input type="number"  name="number" class="mdc-text-field__input" autocomplete="off" required>
										<button class="material-icons mdc-text-field__icon"  tabindex="0"  type="submit" >edit</button>
										<div class="mdc-notched-outline">
											<div class="mdc-notched-outline__leading"></div>
											<div class="mdc-notched-outline__notch">
												<label for="tf-outlined" class="mdc-floating-label">phone Number</label>
											</div>
											<div class="mdc-notched-outline__trailing"></div>
										</div>
									</div>
									<button type=submit class="shadow-none mdc-button mdc-button--unelevated  mdc-button--outlined my-custom-button ml-0 ml-md-5 mt-2 mt-md-0 "  >
										<span class="mdc-button__label ">save</span>
									</button>
								</div>

							</form>
							<div class="modal-footer" style="border:none;">

							</div>

						</div>
					</div>

				</div>
			</div>
    </div>

  </div>


</div>


<div class="mdc-snackbar"  style="    z-index: 1000000000;">
  <div class="mdc-snackbar__surface">
    <div class="mdc-snackbar__label"	role="status" aria-live="polite"></div>
    <div class="mdc-snackbar__actions">
      <button type="button" class="mdc-button mdc-snackbar__action">ok</button>
    </div>
  </div>
</div>

@if($errors->any())
  <div class="mdc-snackbar mdc-snackbar-skill"  style="    z-index: 1000000000;">
    <div class="mdc-snackbar__surface">
      <div class="mdc-snackbar__label" role="status" aria-live="polite">
        {{$errors->first()}}
      </div>
      <div class="mdc-snackbar__actions">
        <button type="button" class="mdc-button mdc-snackbar__action">ok</button>
      </div>
    </div>
  </div>
@endif

<script type="text/javascript">
// jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#skill').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('skills.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'skill':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)
          $('#skill_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#skill_list li', function(){
      // declare the value in the input field to a variable
      var value = $(this).text();
      // assign the value to the search box
      $('#skill').val(value);
      // after click is done, search results segment is made empty
      $('#skill_list').html("");
    });
  });
</script>


<script type="text/javascript">
  // jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#contact').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('contacts.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'contact':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)

          $('#contact_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#contact_list li', function(){

      // declare the value in the input field to a variable
      var value = $(this).text();

      // assign the value to the search box
      $('#contact').val(value.split(' ')[0]);
      // after click is done, search results segment is made empty

      $('#contact_list').html("");
      $('#contactIdInput').html('	<input name="contactId" id=contactId type="hidden" value="'+$(this).children().text()+'" >');

    });
  });
</script>

@endsection
