@extends('layouts.app')
<script src="{{ asset('js/visaSimulator.js') }}" defer></script>
@section('content')

<style>
@media (min-width: 830px){
	.page-header {

		height: 100vh;
		background-position: center center;
		background-size: cover;
		background-image: url("{{URL::asset('img/faq.jpg')}}");
		display: flex;
		align-items: center;

	}

}
</style>

<div class="container-fluid page-header"  >

	<div class="container pt-5">





		<div class="position-relative overflow-hidden  text-white row py-5" >
			<div class="col-md-5 col-sm-12"></div>
			<div class="col-md-7 " >

				<h1 class=" mdc-typography--headline2 font-weight-bold  py-2 text-left">{{ __('faqMessages.title') }}</h1>
				<hr class="white-hr">
				<div class=" font-weight-normal mdc-typography--body1 text-justify">{{ __('faqMessages.intro') }}</div>

				<div class="product-device box-shadow d-none d-md-block"></div>
				<div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
			</div>

		</div>
	</div>
</div>

<main class="bg-white shade py-5"  >

	<div class="container">

		<div class="row">

			<div class="col-12">
				<p class="text-left  mdc-typography--headline4 primary font-weight-bold">
					{{ __('faqMessages.text1') }}
				</p>
				<hr>
				<p class="text-left  mdc-typography--button font-weight-bold primary ">
					{{ __('faqMessages.text2') }}
				</p>
				<p class="text-left  mdc-typography--body2">
					{{ __('faqMessages.response1') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text3') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response2') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.newtext1') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.newresponse1') }}
        </p>
        <br>
        <br>
				<p class="text-left  mdc-typography--headline4 primary font-weight-bold">
					{{ __('faqMessages.text4') }}
				</p>
					<hr>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text5') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response3') }}<a class="links-faq" href="mailto:contact@tai2tai.com">contact@tai2tai.com</a>.
				</p>

				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.newtext2') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.newresponse2') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text6') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response4') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text7') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response5') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text8') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
          {{ __('faqMessages.response6') }}
          <br>
          {{ __('faqMessages.response6-1') }}
          <div class="container">
            <ol>
              <li>{{ __('faqMessages.response6-1-1') }}</li>
              <li>{{ __('faqMessages.response6-1-2') }}</li>
              <li>{{ __('faqMessages.response6-1-3') }}</li>
              <li>{{ __('faqMessages.response6-1-4') }}</li>
              <li>{{ __('faqMessages.response6-1-5') }}</li>
              <li>{{ __('faqMessages.response6-1-6') }}</li>
              <li>{{ __('faqMessages.response6-1-7') }}</li>
              <li>{{ __('faqMessages.response6-1-8') }}</li>
              <li>{{ __('faqMessages.response6-1-9') }}</li>
            </ol>
          </div>
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text9') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response7') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text10') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response8') }}
				</p>

				<p class="text-left  mdc-typography--button font-weight-bold primary ">
					{{ __('faqMessages.text11') }}
				</p>

				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response9') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text12') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response10') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text13') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response11') }}
				</p>

				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text14') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
          {{ __('faqMessages.response12') }}
          <a class="links-faq" href="{{URL::asset('img/info-graph.png')}}" download>download
            <img src="{{URL::asset('img/download.png')}}" width="20" style="margin-top: -5px; margin-left: -5px;"/>
          </a>
          {{ __('faqMessages.response12-1') }}
				</p>
				<p class="text-left  mdc-typography--headline4 primary font-weight-bold">
					{{ __('faqMessages.text15') }}
				</p>
				<hr>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text16') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response13') }}
				</p>
				<p class="text-left  mdc-typography--button font-weight-bold primary">
					{{ __('faqMessages.text17') }}
				</p>
				<p class="text-left  mdc-typography--body2 ">
					{{ __('faqMessages.response14') }}
				</p>
			</div>
		</div>
	</div>


</main>


@endsection
