
@extends('layouts.app')

<script src="{{ asset('js/profile.js') }}" defer></script>

@section('content')

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
  });
  jQuery(document).ready(function($) {
    $(".invoice").click(function() {
        window.location = $(this).data("href");
    });
  });
</script>

<style>
  .row-table {
    transition: 0.25s ease;
    cursor: pointer;
  }

  .row-table:hover {
    transform: scale(1.01);
    background-color: #c5f2f9 !important;
  }

</style>

<div class="container-fluid bg-white">

  {{-- start name and email section --}}


	<div class="row bg-primary py-5">
		<div class="col-12">
			<h1 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">{{ \Auth::user()->name }}</h1>
		</div>
		<div class="col-12">
			<h3 class="mdc-typography--headline4 text-center text-capitalize font-weight-bold  text-white">Quick Invoices</h3>
		</div>
		<div class="col-12">
			<div class=" mdc-typography--button  text-center text-white">

				{{ \Auth::user()->email }}
				{{-- {{ dd(\Auth::user())}} --}}
      </div>

		</div>
  </div>


  {{-- end name and email section --}}

	<div>
    {{-- start contract display section --}}


		<main class="bg-white container-fluid py-5">

			<div class="row">


				<div class="col-md-12 col-sm-12 mx-auto ">
          <div class="d-flex justify-content-between">
            <div class=" mt-1 mb-3  ">
              <a class="btn btn-primary" href="{{ route('profile.show', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                go to Contracts
              </span>
              </a>
            </div>
            <div class=" mt-1 mb-3 ">
              <a class="" href="{{ route('quickInvoice.index', ['user' => Auth::id()]) }}">
                <span class=" mdc-typography--button">
                Quick invoices
              </span>
              </a>
            </div>
          </div>

            {{-- {{ dd(Auth::user()->role->name) }} --}}

          <div id="demo">
            <div class="table-responsive-vertical shadow-z-1">
            <!-- Table starts here -->
              <table id="table" class="table table-hover table-mc-light-blue">
                <thead>
                  <tr>
                    <th class="font-weight-bold mdc-typography--body1">Invoice No.</th>
                    <th class="font-weight-bold mdc-typography--body1">Talents</th>
                    <th class="font-weight-bold mdc-typography--body1">Clients</th>
                    <th class="font-weight-bold mdc-typography--body1">Company</th>
                    <th class="font-weight-bold mdc-typography--body1">Gross payment</th>
                    <th class="font-weight-bold mdc-typography--body1">VAT rate</th>
                    <th class="font-weight-bold mdc-typography--body1">Gross payment W/O VAT</th>
                    <th class="font-weight-bold mdc-typography--body1">Invoice date</th>
                    <th class="font-weight-bold mdc-typography--body1">Payment status</th>
                    <th class="font-weight-bold mdc-typography--body1">Invoice Payment date</th>
                    <th class="font-weight-bold mdc-typography--body1">Paid to talent</th>
                  </tr>
                </thead>
                <tbody>

                  {{-- {{ dd(Auth::user()->quickInvoices) }} --}}

                  @if (Auth::user()->role->name =='user')

                  @foreach (Auth::user()->quickInvoices->reverse() as $quickInvoice)



                  <tr class='clickable-row row-table' data-href='/quick-invoice/{{ $quickInvoice->id }}'>
                    <td class="mdc-typography--body1">{{$quickInvoice->id}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->talent->name}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->clients}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->company}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->gross_payment}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->vat_rate}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->gross_payement_without_vat}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->invoice_date}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->payement_status}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->invoice_payment_date}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->paid_to_talent}}</td>
                  </tr>





                  @endforeach


                  @else

                  @forelse ($quickInvoices->reverse() as $quickInvoice)

                  <tr class='clickable-row row-table' data-href='/quick-invoice/{{ $quickInvoice->id }}'>
                    <td class="mdc-typography--body1">{{$quickInvoice->id}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->talent->name}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->clients}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->company}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->gross_payment}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->vat_rate}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->gross_payement_without_vat}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->invoice_date}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->payement_status}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->invoice_payment_date}}</td>
                    <td class="mdc-typography--body1">{{$quickInvoice->paid_to_talent}}</td>
                  </tr>

                  @empty
                    <tr>
                      <td colspan="11" class="font-weight-bold mdc-typography--body1 text-center">no Invoice</td>
                    </tr>
                  @endforelse
                  @endif
                </tbody>
              </table>
            </div>
          </div>

            <div class="row justfy-content-end">
						<div class="col" align=right>
              <a href="/admin/quick-invoices/create" target="_blank">
                <button class="mdc-fab mr-5 my-fab " aria-label="Favorite"   style="margin-top:-30px;">
                  <span class="mdc-fab__icon material-icons">add</span>
                </button>
              </a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</main>

    {{-- end contract display section --}}






  </div>


</div>


<script type="text/javascript">
// jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#skill').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('skills.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'skill':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)
          $('#skill_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#skill_list li', function(){
      // declare the value in the input field to a variable
      var value = $(this).text();
      // assign the value to the search box
      $('#skill').val(value);
      // after click is done, search results segment is made empty
      $('#skill_list').html("");
    });
  });
</script>


<script type="text/javascript">
  // jQuery wait till the page is fullt loaded
  $(document).ready(function () {
    // keyup function looks at the keys typed on the search box
    $('#contact').on('keyup',function() {
      // the text typed in the input field is assigned to a variable
      var query = $(this).val();
      // call to an ajax function
      $.ajax({
        // assign a controller function to perform search action - route name is search
        url:"{{ route('contacts.fetch') }}",
        // since we are getting data methos is assigned as GET
        type:"GET",
        // data are sent the server
        data:{'contact':query},
        // if search is succcessfully done, this callback function is called
        success:function (data) {
          // print the search results in the div called skill_list(id)

          $('#contact_list').html(data);
        }
      })
      // end of ajax call
    });

    // initiate a click function on each search result
    $(document).on('click', '#contact_list li', function(){

      // declare the value in the input field to a variable
      var value = $(this).text();

      // assign the value to the search box
      $('#contact').val(value.split(' ')[0]);
      // after click is done, search results segment is made empty

      $('#contact_list').html("");
      $('#contactIdInput').html('	<input name="contactId" id=contactId type="hidden" value="'+$(this).children().text()+'" >');

    });
  });
</script>

@endsection
