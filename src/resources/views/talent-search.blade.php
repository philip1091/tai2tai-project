@extends('layouts.app')

@section('content')


 <main class="bg-white shade pb-5" style="    transform: translate3d(0px, 0px, 0px);" >


    <div class="container text-dark mb-5"  >
      <div class="row ">
        <div class="p-lg-4 mx-auto mt-5">
          <h1 id="talent-title" class="mdc-typography--headline4 font-weight-bold primary text-center mb-5">Tai2tai's Current Talents</h1>

        </div>
      </div>


      <div class="container ">
        <div class="row">
          <div class="col-md-12 mx-auto mb-5">
            <form method="get" action="/talent-presentation/search">
              <div class="text-center d-flex justify-content-center align-content-center ">
                <input id="search-talent" name="input" style="width: 100%; height: 40px;" class="mdc-typography--body1 col-md-8 mr-3" type="text" id="searchTalent" class="form-control" placeholder="Search by Keyword..." autocomplete="off" value="{{$s}}">
                <button class="mdc-typography--body1 btn btn-primary mr-3" type="submit">Search</button>
                <a  style="width: fit-content;" href="/talent-presentation" class="mdc-typography--body1 btn btn-danger" type="reset">Reset search</a>
              </div>
              <div class="mb-5 text-center">
                @error('input')
                  <small  style="font-size: 0.8rem !important;"  class="mdc-typography--body1 text-danger">{{$message}}</small>
                @enderror
              </div>
            </form>
          </div>
        </div>
      </div>


      <div class="mb-4 text-center">

        @if (  $talents->count() == 1 )
        <p class="mdc-typography--body1 "> There is {{$talents->count()}} result for "<strong> {{$s}} </strong>"</p>
        @elseif($talents->count() > 1)
        <p class="mdc-typography--body1 "> There are {{$talents->count()}} results for "<strong> {{$s}} </strong>"</p>
        @endif
      </div>

      @forelse ($talents as $talent)

      <div class="card card-talent mb-3" style="box-shadow: 0 0 15px rgba(0,0,0,0.2); height: fit-content;">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 bg-primary">
            <img class="talent-img" src="{{ Voyager::image($talent->image )}}" style="height: 400px; width: 100%; object-fit: cover;" alt="Talent image">
            <div class="text-center text-warning">
              <h2 class="mdc-typography--headline3 mt-2">{{ $talent->name }}</h2>
              <p class="mdc-typography--headline5">{{ $talent->job_title}}</p>
            </div>
          </div>
          <div class="col-md-1 col-sm-12 px-0"></div>
          <div class="col-lg-4 col-md-4 mt-5 spacing-small-screen">
            <div class="mb-5">
              <h2 class="mdc-typography--headline7 text-primary">HARD SKILLS</h2>
              @foreach ( $talent->hardskills as $hardskill)
              <li class="mdc-typography--body1 text-muted">{{ $hardskill->hard_skill }}</li>
              @endforeach
            </div>
            <div class="mb-5">
              <h2 class="mdc-typography--headline7 text-primary">SOFT SKILLS</h2>
              @foreach($talent->softskills as $softskill)
              <li class="mdc-typography--body1 text-muted">{{ $softskill->soft_skill }}</li>
              @endforeach
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12  mt-5 spacing-small-screen">
            <div class="mb-5">
              <h2 class="mdc-typography--headline7 text-primary">EXPERIENCES</h2>
              <p class="mdc-typography--body1 text-muted">{{ $talent->experience}}</p>
            </div>
            <div class="mb-5">
              <h2 class="mdc-typography--headline7 text-primary">KEY INDUSTRIES</h2>
              @foreach($talent->keyindustries as $key)
              <li class="mdc-typography--body1 text-muted">{{ $key->key_industry }}</li>
              @endforeach
            </div>
            <div class="mb-5">
              <h2 class="mdc-typography--headline7 text-primary">LANGUAGE PROFICIENCY</h2>
              @foreach($talent->languages as $language)
              <li class="mdc-typography--body1 text-muted">{{ $language->language }}</li>
              @endforeach
            </div>
          </div>
        </div>
      </div>

      @empty

        {{-- <p class="mdc-typography--body1 text-center">There are no talents to show for the moment</p> --}}
        <p class="mdc-typography--body1 text-center">There are no talents related with the keyword "<strong>{{$s}} </strong>" </p>

      @endforelse

      {{-- <div class="col-12 text-center">
				{{ $talents->appends(['s' => $s])->links() }}
			</div> --}}

    </div>

  </main>

@endsection
