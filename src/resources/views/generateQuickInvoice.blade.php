<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Invoice</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .quick-invoice{
          margin: 30px 250px 15px 250px;
          border: 1px solid #898989;
          background: white;
          padding: 48px;
        }

        .text-blue{
          color: #0092AD;

        }

        .stick-right {
          margin-left: 72px;
        }

        .text-right {
            text-align: right;
        }
        .text-left{
            text-align: left;
        }
        .resize {
          max-width:60%;
          max-height:60%;
        }

    </style>

</head>
<body class="login-page " style="background:#ffffff; margin: 0px 25px;">


      <div class="row">
        <div class="col-xs-4">
          <img class="resize" src="{{url('/img/logoTai2.png')}}" alt="logo">
        </div>
      </div>
      <div class="row text-center">
        <h2 class="text-blue">Invoice</h2>
      </div>
        <div class="row">
            <div class="col-xs-7">
                <br><strong style="line-height: 30px;">From: Gregoire Giroux</strong>
                <br>
                <span style="line-height: 30px;">
                  with <strong>TAI2</strong><br>
                </span>
                <span style="line-height: 30px;">
                  F3097,Building 1, No.5500,Yuanjiang Rd,Minhang District, Shanghai
                </span>
                <br>
                <strong>E</strong>: gregoire.giroux@tai2tai.com
                <br>
            </div>


        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>

        <div class="row">
            <div class="col-xs-6">
              <span >
                <strong>To: </strong> {{ $quickInvoice->clients }}<br>
              </span>
                    <span >{{ $quickInvoice->company }}</span> <br>
                    <span >P : {{ $quickInvoice->client_phone }}</span> <br>
                    <span >{{ $quickInvoice->client_address }}</span> <br>

            </div>

            <div class="col-xs-5 ">
                <table style="width: 100%">
                    <tbody>
                        <tr style="line-height: 60px;">
                            <th>Invoice Num:</th>
                            <td class="text-right">{{$quickInvoice->id}}</td>
                        </tr>
                        <tr>
                            <th> Invoice Date: </th>
                            <td class="text-right">{{$quickInvoice->invoice_date}}</td>
                        </tr>

                    </tbody>
                </table>

                <div style="margin-bottom: 0px">&nbsp;</div>
            </div>
        </div>

        <table class="table">
            <thead style="background: #F5F5F5;">
              <tr>
                <th>Service List</th>
                <th>type</th>
                <th>unit</th>
                <th>per&nbsp;unit&nbsp;price</th>
                <th class="text-right">Price</th>
              </tr>
            </thead>
            <tbody>
                {{-- @foreach($quickInvoice->Service as $service) --}}
                  <tr>
                    <td><div><strong>{{ $quickInvoice->service_name }}</strong></div></td>
                    <td>{{ $quickInvoice->service_type }}</td>
                    <td>{{ $quickInvoice->service_unit }}</td>
                    <td>{{ $quickInvoice->per_unit_price }}</td>
                    <td class="text-right">{{ $quickInvoice->service_price }}</td>
                  </tr>
                {{-- @endforeach --}}
                <tr>
                </tr>
            </tbody>
        </table>

            <div class="row">
                <div class="col-xs-6"></div>
                <div class="col-xs-5 ">
                    <table style="width: 100%">
                        <tbody>
                          <tr class="well" style="padding: 5px">
                              <th style="padding: 5px"><div> Balance Due Tax Free ({{$quickInvoice->currency}})</div></th>
                              <td style="padding: 5px" class="text-right"><strong>{{$quickInvoice->service_price}}</strong></td>
                          </tr>
                          <tr style="padding: 5px">
                              <th style="padding: 4px"><div> VAT ({{ $quickInvoice->vat_rate }}) ({{$quickInvoice->currency}})</div></th>                                          <!-- TAX IS HERE FOR THE FACTURATION-->
                              <td style="padding: 5px" class="text-right">{{$quickInvoice->vat_amount}}</td>
                          </tr>
                          <tr class="well" style="padding: 5px">
                              <th style="padding: 5px"><div> Balance Due With VAT({{$quickInvoice->currency}})</div></th>
                              <td style="padding: 5px" class="text-right"><strong>{{$quickInvoice->gross_payment}}</strong></td>
                          </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div style="margin-bottom: 0px">&nbsp;</div>

            <div class="row ">

                <div class="col-xs-8 ">
                  <strong style="line-height: 40px;">Payment Term :</strong><br>
                  {{$quickInvoice->payment_term}}.<br>
                  <br>

                  <div class="text-left">
                    <strong style="line-height: 40px;">Payment by Alipay</strong><br>
                    <img style="max-width:20%;" src="{{url('/img/alipay-qr.png')}}" alt="alipay_qr_code">
                  </div>

                  <strong style="line-height: 40px;">Payment by Chinese Bank</strong><br>
                  Beneficiary's Name: <img style="width:40%;" src="{{url('/img/ch-1.png')}}" alt="alipay_qr_code"><br>
                  Bank Account Number: 440377782325<br>
                  Beneficiary's Banker Name: <img style="width:55%;" src="{{url('/img/ch-2.png')}}" alt="alipay_qr_code"><br>
                  CNAPS Code: 104290070069
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>

                  <div>
                    <strong style="line-height: 40px;">Payment by Foreign bank</strong><br>
                    Beneficiary's Name: Shanghai TAI2TAI Consulting Co.,LTD.<br>
                    Bank Account Number: 440377782325<br>
                    Beneficiary's Banker Name: Bank of China Shanghai GongHeXin Road Sub-Branch<br>
                    Beneficiary's Banker Address: Room 109, No.1982, GongHexin Road, Jingan District, Shanghai, China<br> SWIFT Code: BKCHCNBJ300<br>
                    Post Code: 200072<br>
                    CNAPS Code: 104290070069
                  </div>
                  <br><br>



                  <p>
                    <strong>
                      {{ $quickInvoice->talent->name }} mission:
                    </strong>
                    <br>
                    {{ $quickInvoice->talent_mission }}
                  </p>
                </div>


            </div>



          </body>
          </html>
          <div class="text-center" style="margin-bottom: 30px;">
            <a href="/quick-invoice-download/{{ $quickInvoice->id }}" class="btn btn-primary" type="button">Download Invoice PDF</a>
          </div>
