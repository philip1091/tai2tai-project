@extends('layouts.app')

@section('content')


{{--  start section with bcg img --}}

<div class="container-fluid page-header-about "  style="height: 600px; padding-left: 40px;">

	<div class="container pt-5">

		<div class="position-relative overflow-hidden  text-dack row py-5" >
			<div class="col-md-5 col-sm-12"></div>
			<div class="col-md-7 " >

				<h1 class=" mdc-typography--headline2 font-weight-bold   py-2 text-left">{{ __('aboutUsMessages.title') }}</h1>
				<hr>
				<div class=" font-weight-normal mdc-typography--body1 text-justify">{{ __('aboutUsMessages.intro') }}</div>

				<div class="product-device box-shadow d-none d-md-block"></div>
				<div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
			</div>

		</div>
	</div>
</div>

{{-- end section with bcg img --}}

<main class="bg-white shade py-5"  >

{{-- start of bio section --}}

	<div class="container">
		<div class="row">
			<div class="col-12">
				<p class="text-center  mdc-typography--headline4  font-weight-bold primary pb-5 mt-5">
					{{ __('aboutUsMessages.text0') }}
				</p>
			</div>
    </div>

    {{-- start of ALM bio  --}}
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/alexia-le-masson/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text1') }} <br>
            </p>

            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio1') }}
            </p>

            <div class="email-format">
              <div class="inline-block">Email:</div>
              <div class="inline-block primary mdc-typography--body1">{{ __('aboutUsMessages.email1') }}</div>
            </div>


          </div>
          <div class="col-md-4 col-sm-12 text-center ">
            <img src="{{URL::asset('img/alexia.jpg')}}" width="300" class=" " />
          </div>
        </div>
      </a>
    </div>
    <br>



     {{-- end of ALM bio  --}}

     {{-- start of GG bio  --}}

    <br>
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/ggiroux/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-4 col-sm-12 text-center mt-4">
            <img src="{{URL::asset('img/greg.jpeg')}}" width="250" class=" " />
          </div>
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text2') }}
            </p>
            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio2') }}
            </p>

            <div class="email-format">
              <div class="inline-block">Email:</div>
              <div class="inline-block primary mdc-typography--body1">{{ __('aboutUsMessages.email2') }}</div>
            </div>

          </div>

        </div>
      </a>
    </div>
    <br>

    {{-- end of GG bio  --}}

    {{-- start of GL bio  --}}

    <br>
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/gregorylpz/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text3') }}
            </p>
            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio3') }}
            </p>

            <div class="email-format">
              <div class="inline-block">Email:</div>
              <div class="inline-block primary mdc-typography--body1">{{ __('aboutUsMessages.email3') }}</div>
            </div>

          </div>
          <div class="col-md-4 col-sm-12 text-center ">
            <img src="{{URL::asset('img/gl.jpeg')}}" width="250" class=" " />
          </div>
        </div>
      </a>
    </div>
    <br>



     {{-- end of GL bio  --}}

     {{-- start of Jess bio  --}}

    <br>
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/jessica-zhang-526a4b168/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-4 col-sm-12 text-center ">
            <img src="{{URL::asset('img/jess.jpeg')}}" width="250" class=" " />
          </div>
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text4') }}
            </p>
            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio4') }}
            </p>

            <div class="email-format">
              <div class="inline-block">Email:</div>
              <div class="inline-block primary mdc-typography--body1">{{ __('aboutUsMessages.email4') }}</div>
            </div>

          </div>

        </div>
      </a>
    </div>

    {{-- end of Jess bio  --}}

    {{-- start of MAX bio  --}}

    {{-- <br>
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/maxime-ponsan-49937a162/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text5') }}
            </p>
            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio5') }}
            </p>

          </div>
          <div class="col-md-4 col-sm-12 text-center ">
            <img src="{{URL::asset('img/max.png')}}" width="250" class=" " />
          </div>
        </div>
      </a>
    </div>
    <br> --}}



     {{-- end of MAX bio  --}}

     {{-- start of Phil bio  --}}

    <br>
    <div class="card about-card py-4">
      <a href="https://www.linkedin.com/in/philip1091/" target="_blank" >
        <div class="row mx-auto">
          <div class="col-md-8 col-sm-12">
            <br>
            <p class="text-right  mdc-typography--headline5 text-sm-left font-weight-bold primary">
              {{ __('aboutUsMessages.text6') }}
            </p>
            <br>
            <p class="text-right  mdc-typography--body1 text-sm-left">
              {{ __('aboutUsMessages.bio6') }}
            </p>
            <div class="email-format">
              <div class="inline-block">Email:</div>
              <div class="inline-block primary mdc-typography--body1">{{ __('aboutUsMessages.email6') }}</div>
            </div>

          </div>
          <div class="col-md-4 col-sm-12 text-center ">
            <img src="{{URL::asset('img/phil.jpg')}}" width="250" class=" " />
          </div>
        </div>
      </a>
    </div>
    <br>



     {{-- end of Phil bio  --}}
	</div>


</main>


@endsection
