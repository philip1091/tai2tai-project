@extends('layouts.app')

@section('content')


<div class="container-fluid   page-header-talent"style="background-image:linear-gradient(rgba(0, 0, 0, .5), #0505058f),url('../img/talentsPage.jpg'); height: 600px;" >
	<div class="container  text-white ">
		<div class="row justify-content-end py-5"  >

			<div class="row py-md-5 py-sm-1">
				<div class="col-md-5 col-sm-12"></div>
				<div class="col-md-7 col-sm-12 my-5 py-md-5 py-sm-1  " >

					<h1 class="mdc-typography--headline2 text-left  font-weight-bold ">{{ __('talentsMessages.title') }} </h1>

          <p class="mdc-typography--body1 text-justify text-left pb-3">{{ __('talentsMessages.intro') }}</p>
          <a  class="text-center" href="pdf/Talent-Presentation.pdf" target="_blank">
            <button type="button" class="btn btn-outline-light">
              <span class="mdc-typography--body1">
                {{ __('talentsMessages.button') }}
              </span>
            </button>
          </a>

				</div>
			</div>
		</div>
	</div>
</div>


<main class="bg-white shade pb-5" style="    transform: translate3d(0px, 0px, 0px);" >


	<!-- FOOTER -->






	<div class="container text-dark ">
		<div class="row "  >
			<div class="p-lg-4 mx-auto mt-5">
				{{-- <h1 class="mdc-typography--headline4 font-weight-bold primary text-center">{{ __('talentsMessages.title2') }}</h1> --}}
				<h1 class="mdc-typography--headline4 font-weight-bold primary text-center">{{ __('talentsMessages.subtitle2') }}
				</h1>
			</div>
		</div>
		<div class="row "  >

			<div class="col-md-1 col-sm-12 " >
				<img src="{{URL::asset('img/logoSimple.png')}}" width="80" />
			</div>
			<div class="col-10 " >
				<p class="mdc-typography--body1  my-4 text-left primary">{{ __('talentsMessages.blue1') }}
					<span class="text-dark">{{ __('talentsMessages.black1') }}
					</span>
				</p>
			</div>

		</div>
		<div class="row "  >

			<div class="col-md-1 col-sm-12 " >
				<img src="{{URL::asset('img/logoSimple.png')}}" width="80" />
			</div>
			<div class="col-10 " >
				<p class="mdc-typography--body1  my-4 text-left primary">{{ __('talentsMessages.blue2') }}
					<span class="text-dark">{{ __('talentsMessages.black2') }}
					</span>
				</p>
			</div>

		</div>
		<div class="row "  >

			<div class="col-md-1 col-sm-12" >
				<img src="{{URL::asset('img/logoSimple.png')}}" width="80" />
			</div>
			<div class="col-10 " >
				<p class="mdc-typography--body1  my-4 text-left primary">{{ __('talentsMessages.blue3') }}
					<span class="text-dark">{{ __('talentsMessages.black3') }}
					</span>
				</p>
			</div>

		</div>
		<div class="row "  >

			<div class="col-md-1 col-sm-12 " >
				<img src="{{URL::asset('img/logoSimple.png')}}" width="80" />
			</div>
			<div class="col-10 " >
				<p class="mdc-typography--body1  my-4 text-left primary">{{ __('talentsMessages.blue4') }}
					<span class="text-dark">{{ __('talentsMessages.black4') }}
					</span>
				</p>
			</div>

		</div>
		<div class="row "  >

			<div class="col-md-1 col-sm-12" >
				<img src="{{URL::asset('img/logoSimple.png')}}" width="80" />
			</div>
			<div class="col-10 " >
				<p class="mdc-typography--body1  my-4 text-left primary">{{ __('talentsMessages.blue5') }}
					<span class="text-dark">{{ __('talentsMessages.black5') }}
					</span>
				</p>
			</div>

		</div>
		<div class="row "  >
			<div class="p-lg-4 mx-auto mt-5">
				<h1 class="mdc-typography--headline4 font-weight-bold primary text-center">{{ __('talentsMessages.title3') }}</h1>

			</div>
		</div>
		<div class="row d-flex justify-content-around "  >
			<div class="col-md-4 col-sm-12 " >

				<div class="position-relative overflow-hidden text-center ">
					<div class=" mx-auto ">
						<a class="" href="/faq" >
							<img src="{{URL::asset('img/faq.png')}}" width="80" />
						</a>
						<p class="mdc-typography--body1 text-justify text-center my-4">{{ __('talentsMessages.faq') }}
						</p>

					</div>

				</div>

			</div>

			<div class="col-md-4 col-sm-12 ">

				<div class="position-relative overflow-hidden text-center ">

					<a  href="pdf/talent-flyer.pdf" target="_blank">
            {{-- <img src="{{URL::asset('img/mail.png')}}"  class="pt-4" width="100" /> --}}
             <img src="{{URL::asset('img/pdf.png')}}"class="pt-3" width="80" />
					</a>
					<p class="mdc-typography--body1 text-justify text-center my-4">{{ __('talentsMessages.text2') }}
					</p>

				</div>


			</div>
		</div>

	</div>

</main>

@endsection
