<?php

return [
	'img1' => ' ',
	'img2' => ' ',
	'intro' => 'Co-founders of Tai2Tai, we have been in Shanghai for 2 and 3 years respectively.  Motivated by an  observation: too many people around us did not have the right opportunity to make the most of their talents and skills, we have decided to start Tai2Tai. To explain this situation, we have identified two main reasons directly connected to expat status. First, following a spouse is a great aventure but it may also be a great challenge to take your career to the next step: «Am I going to find a exciting job here? » «Will I stand not having my own money? » or « Isn’t it the right timing to switch career path?». The second reason may be that finding the right balance between parenthood and professional life can be a struggle, especially that we are away from family and friends.  Tai2Tai wishes to offer various consulting expertise, a supporting community and a genuine place to learn and grow.
	',
	'title' => 'The people behind Tai2tai
	',
	'text0' => 'Our team
	',


  'text1' => 'Alexia le Masson  - Co-Founder ',

	'email1' => 'alexia.lemasson@tai2tai.com',

  'bio1' =>'Alexia le Masson co-founded Tai2Tai in 2019, a consulting company based in Shanghai. As the Managing Director, she connects our amazing Talents to Clients who want to have a time-based mission completed in China.  Her passion for Client Satisfaction can be traced back to her jobs in luxury hospitality and event planning. How does Tai2Tai fit into it? Well, to her, there is nothing more thrilling than getting talents to achieve fulfillment and, clients to expand their business thanks to us.
  Outside of work, don’t challenge her to rugby trivia, her passion for this sport is endless, once as a player, and now as a fan! Because there is nothing we can’t tackle!
  Connect with Alexia over LinkedIn or WeChat for networking, exploring business opportunities in China or offering a commentary to a rugby game! ',

  'text2' => 'Grégoire GIROUX  - Co-Founder ',
  'email2' => 'gregoire.giroux@tai2tai.com',

  'bio2' =>'Grégoire Giroux is a finance executive with over 10 years of international experience in retail, manufacturing and IT. He specialises in helping companies scale up through financial structuring and transformation. He co-founded Tai2tai in 2019 with Alexia Lemasson to help bring short term talent and expertise to other companies looking to scale up in China.
  Gregoire arrived in China in 2014 to become China Chief Financial Officer of SNF based in Taixing, Jiangsu Province.  He then moved to Shanghai as APAC Chief Financial Officer of Id Group, and later Chief Financial Officer of IT Consultis.
  Before moving to Shanghai Grégoire held various positions in finance and consulting in Paris and London. He holds Master degrees in engineering from École Nationale Supérieure d’Informatique pour l’Industrie et l’Entreprise (ENSIIE) and an MBA from l’ESSEC business school.',

  'text3' => 'Gregory Lopez  - Associate ',
  'email3' => 'gregory.lopez@tai2tai.com',

  'bio3' =>'Gregory Lopez has been in IT for over 25 years. He began his career in France, to quickly decide to live in London. He then joined a large industrial group in critical systems to manage European projects and programs. Extensive experience in China to coordinate research and innovation programs between Paris, Shanghai and Beijing. He then participated in international deployment programs in Europe and the United States for large groups.
  Passionate about travel, sport, he is deeply involved in several charity events for the French and Chinese community.',

  'text4' => 'Jessica Zhang  - Office Manager ',
  'email4' => 'jessica.zhang@tai2tai.com',

  'bio4' =>'Jessica Zhang is the office manager at Tai2Tai. She is managing the financial and administrative departments. The Chinese Tax Bureau and the Visa Bureau hold no secret for her. What makes her unique for Tai2Tai? Well, she does not fear to go out of her comfort zone. She always dares to try and continuously wants to learn and, understand things.
  Jessica is a stronger believer in the power of positive thinking, and thus she’s really into Yoga. Outside of work, you can find her enjoying a good movie, or on the hilly countryside roads, biking.',

  'text5' => 'Maxime Ponsan  - Office Manager ',

  'bio5' =>'Admitted to the Paris Bar in 2014, Mr. Maxime Ponsan holds a 2nd Year Master’s degree in Business & Tax Law Paris II Panthéon-Assas and is a graduate of the ESSEC Grande École.
He began his career in the corporate law department of Linklaters LLP in Paris in 2014 before joining UGGC in 2015.',

  'text6' => 'Jean Philippe Hicintuka  - IT Intern',
  'email6' => 'jeanphilh@outlook.com',

  'bio6' =>'Jean Philippe studied in Shanghai for the three years and obtained his Master degree in Information and Communication Engineering from Donghua Univsersity in January 2020. He started working as
  a freelancer then join Tai2Tai as a Fullstack developer Intern in May 2020, He is a higly driven Web developer, passionate with coding, making web apps and eager to learn and gain more experience in the IT field.',


];
