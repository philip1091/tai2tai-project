<?php

return [
	'img1' => ' ',
	'img2' => ' ',
	'intro' => 'Please read carefully our FAQ page to find answers to your questions. However, if you can’t find the right information, please do not hesitate to drop us a line at contact@tai2tai.com. We will happily get back to you within 48 hours! 	',
	'title' => 'FAQ - everything there is to know about tai2tai

	',
	'text1' => 'Hiring a consultant
	',
	'text2' => 'What kind of consultants do you provide?',


	'response1' => 'Tai2Tai is a consulting firm providing all types of services which could assist our clients in growing their business in China: Finance, Design, Web design, Sales & Marketing, etc. However, our services exclude all professions requiring a specific certification such as jobs related to education, recruitment, laws, etc. Drop us a line and find out if your expertise is matching requirements and market needs.',
	'text3' => 'I want to hire a consultant for a specific package or a time based mission, it is possible?',
	'response2' => 'Yes, you can hire a consultant for a specific mission such as creating a new brand identity as well as for long-term missions, e.g. supervising acquisition due diligence and negotiate purchases.
  Bear in mind that as consultants, we complete specific projects/missions and thus, cannot fill a full-time position, e.g. five days a week, 11 months a year, as it would be considered as disguised employment.
  ',
	'newtext1' => 'HOW DO I LEGALLY HIRE A TALENT FOR A MISSION?',
	'newresponse1' => 'Once the mission’s definition and payment terms are all set, Tai2Tai will sign a service agreement with you, the client. Our partner, UGGC lawyers, write our service agreement or if you wish to provide your own, UGGC will proofread it.',
	'text4' => 'Working for tai2tai ',
	'text5' => 'How can I apply to a position in your Company ',
  'response3' => 'We post open positions on our social media pages, but if you want to send us an unsolicited application, please send it to ',


	'newtext2' => 'WHO IS RESPONSIBLE FOR FINDING ME A MISSIONS ? ',
	'newresponse2' => 'Both parties are responsible for finding missions. We strongly encourage Talents to conduct business development actions like networking events, following up with former clients, connecting on Linkedin, etc. On our hand, Tai2Tai’s team is also dedicated to helping to provide missions to Talents. We are members of the French Chamber of Commerce, the Benelux Chamber of Commerce and soon, the Australian Chamber of Commerce.',

	'text6' => 'Is it legal to be a consultant for a foreigner in Shanghai? ',
  'response4' => 'Tai2Tai is a legal company based in Shanghai which provides companies with a broad community of consultants to perform specific missions. All our experts are legally working for us, holding a Working permit and a Resident Permit. As a Shanghai-based company, we hire Shanghai residents and can’t engage foreign residents from other China Cities.',





  'text7' => 'How can I be sure to be eligible for a WP in China?  ',
	'response5' => 'We consider that everyone has a unique path (experiences, diploma, expertise); we encourage you to reach out to us to discuss your professional next step. China’s government has strong requirements for foreigners to work here, but strong does not mean too high!',


  'text8' => 'What documents do you need to process my WP? ',
  'response6' => 'Documents usually depend on the type of visa you are currently holding. e.g. If you now hold a spouse visa, you don’t have to provide a new Medical report. They will use the one already in their system',
  'response6-1' => 'Please note, that government’s requirement may change, the following list is for information purposes only. If needed, we will give you the updated list when starting the process.',
  'response6-1-1' => 'Colour scan of passport.',
  'response6-1-2' => 'Current China Visa and latest entry stamp page.',
  'response6-1-3' => 'A two-inch colour photo.',
  'response6-1-4' => 'Information form: provided by us to collect general information and past work experiences.',
  'response6-1-5' => 'Prior Work Reference letter (Original and Scan, proving at least two years of relevant working experience).',
  'response6-1-6' => 'Non-criminal Record report issued from your country, need to be legalized by China Embassy or your state Consulate in China.',
  'response6-1-7' => 'Applicant’s original diploma, need to be authorized by China Embassy or your state Consulate in China.',
  'response6-1-8' => 'Labour contract: provided by us.',
  'response6-1-9' => 'Medical Report: For 1st applicants, please visit this website to make an appointment. For Apple user, you can also download the SITHC app (http://sithc.shciq.gov.cn).',



	'text9' => ' How long does take a process a work permit?',
	'response7' => 'The duration may vary, but it generally takes around six weeks to issue both working permit and Resident permit. Please note that you are not allowed to work until both are issued.',

  'text10' => 'Do you need to keep my passport during the whole process?Do you need to keep my passport during the whole process?',
	'response8' => 'The process does not require you to leave your passport the entire time; they will only keep it for seven working days while issuing the Resident permit. In the meanwhile, they will issue you a paper allowing you to travel within China mainland only.
',
	'text11' => 'What is the duration of the working permit? ',


  'response9' => 'Our “Work visa” is for 12 months, and renewal will be discussed individually.',

'text12' => 'Can I resign any time ?  ',
	'response10' => 'Of course, as per China law, you can resign any time by giving a 30-day written notice. During your probation period, you only have to provide a three-day notice. After your official last day, Tai2Tai will cancel your work permit within ten days. Please note that you will then cover all expenses linked to your new visa if you need a new one.

 ',
'text13' => 'Can you transfer my WP if I find another job? ',
	'response11' => 'If you find a new challenge in another company, TAI2TAI will do its best to ensure a smooth transfer, cancelling your T2T working permit and Resident permit and providing you with the cancellation certificates.
 ',
'text14' => 'WHY WORKING FOR YOU RATHER THAN STARTING MY OWN FIE (ex: FIE)?',
	'response12' => 'We are not competing against the FIE option; we may be the best option for someone at a precise moment and the other way around. When working as a talent, you can minimize your initial investment, legal responsibility and your monthly Finance and Admin investment in time and money. For more details,
 ',
	'response12-1' => 'our comparative infographic.
 ',
'text15' => 'Perks & Services  ',
'text16' => 'I also need a medical insurance, do you provide it ?',
	'response13' => 'We are working on offering our employees suitable healthcare insurance. Look for more details in months to come!',
'text17' => 'Do you organize event & provide trainings?',
	'response14' => 'At Tai2Tai, we want our employees to learn and grow. We thus provide continuous support to our Talents for Marketing, pricing and business development. Not only are we here to support our Talents but Talents can help each other to open doors, make recommendations, collaborate on specific projects, etc.',


];
