<?php

return [
	'title' => 'Coming together to create your next story.',
	'intro' => 'Whether you are based in China or not (yet), Tai2Tai
will journey with you on your path to success. You
want to assess the Chinese Market, smoothen a
transition or outsource expertise, we have the right
consultant for you! Tai2Tai`s solution is tailored to your
business, whether it is seasonal or if you have a very
specific project to lead or an urgent need to address.',
	'subTitle' => 'Tai2tai`s Current Talents',


];
