<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('english_name');
            $table->string('chinese_name')->nullable();
            $table->string('country');
            $table->string('phone_number')->nullable();
            $table->string('tax_number')->nullable();
            $table->string('capital_number')->nullable();
            $table->string('registration_number')->nullable();
            $table->string('adress')->nullable();
            $table->string('registration_area')->nullable();
            $table->integer('contact_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
