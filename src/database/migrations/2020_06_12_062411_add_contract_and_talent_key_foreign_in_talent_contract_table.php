<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractAndTalentKeyForeignInTalentContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talent_contract', function (Blueprint $table) {
            $table->foreign('contract_id')->references('id')->on('contract');
            // $table->foreign('talent_id')->references('id')->on('talent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_contract', function (Blueprint $table) {
            $table->$table->dropForeign('talent_contract_contract_id');
            // $table->$table->dropForeign('talent_contract_talent_id');
        });
    }
}
