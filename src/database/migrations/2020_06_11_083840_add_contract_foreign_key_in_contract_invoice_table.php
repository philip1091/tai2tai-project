<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractForeignKeyInContractInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_invoice', function (Blueprint $table) {
            $table->foreign('contract_id')->references('id')->on('contract');
            $table->foreign('invoice_id')->references('id')->on('invoice');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_invoice', function (Blueprint $table) {
            $table->$table->dropForeign('contract_invoice_contract_id_foreign');
            $table->$table->dropForeign('contract_invoice_invoice_id_foreign');
        });
    }
}
