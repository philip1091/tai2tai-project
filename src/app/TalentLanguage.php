<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalentLanguage extends Model
{
    protected $guarded = [];
    public function talents() {
      return $this->belongsToMany(Talent::class);
    }
}
