<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Talent extends Model
{
    protected $guarded = [];

  public function category() {
      return $this->belongsTo(TalentCategory::class);
    }
  public function quickInvoice() {
      return $this->hasMany(QuickInvoice::class);
    }
  public function hardskills() {
      return $this->belongsToMany(TalentHardSkill::class);
    }
  public function softskills() {
      return $this->belongsToMany(TalentSoftSkill::class);
    }
  public function keyindustries() {
      return $this->belongsToMany(TalentKeyIndustry::class);
    }
  public function languages() {
      return $this->belongsToMany(TalentLanguage::class);
    }

    public function scopeSearch($query, $s) {
      return $query->whereHas('hardskills', function ($q) use ($s){
                    $q->whereHas('talents', function($q) use($s){
                    $q->where('hard_skill', 'like', '%' .$s. '%');
                    });
                  })
                  ->orwhereHas('softskills', function ($q) use ($s){
                    $q->whereHas('talents', function($q) use($s){
                    $q->where('soft_skill', 'like', '%' .$s. '%');
                    });
                  })
                  ->orwhereHas('keyindustries', function ($q) use ($s){
                    $q->whereHas('talents', function($q) use($s){
                    $q->where('key_industry', 'like', '%' .$s. '%');
                    });
                  })
                  ->orwhereHas('languages', function ($q) use ($s){
                    $q->whereHas('talents', function($q) use($s){
                    $q->where('language', 'like', '%' .$s. '%');
                    });
                  })
                  ->orwhere('name', 'like', '%' .$s. '%')
                  ->orWhere('job_title', 'like', '%' .$s. '%')
                  ->orWhere('experience', 'like', '%' .$s. '%');
    }


}
