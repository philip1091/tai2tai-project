<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalentCategory extends Model
{
    protected $guarded = [];

    public function talents() {
      return $this->hasMany(Talent::class);
    }

}
