<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalentHardSkill extends Model
{
    protected $guarded = [];

    public function talents() {
      return $this->belongsToMany(Talent::class);
    }

    // public function scopeSearch($query, $s) {
    //   return $query->where('hard_skill', 'like', '%' .$s. '%');
    // }
}
