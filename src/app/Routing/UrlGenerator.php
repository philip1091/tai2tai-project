<?php

namespace App\Routing;

class UrlGenerator extends \Illuminate\Routing\UrlGenerator
{
    public function asset($path, $secure=null)
    {
        $secure = $secure ?? env('APP_HTTPS', false);
        app('log')->debug('', [
            'context' => __METHOD__,
            'secure' => $secure,
        ]);
        return parent::asset($path, $secure);
    }

    public function route($name, $parameters=[], $absolute=true)
    {
        return parent::route($name, $parameters, false);
    }
}
