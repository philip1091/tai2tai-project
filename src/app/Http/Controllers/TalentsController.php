<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Talent;
use App\TalentCategory;
use App\TalentHardSkill;
use App\TalentSoftSkill;
use App\TalentKeyIndustry;
use App\TalentLanguage;




class talentsController extends Controller
{
    public function list() {

      $talents = Talent::all();

      return view('talentPresentation', compact('talents')) ;
    }



    public function search(Request $request) {

      $request->validate([
        'input' => 'required|min:2',
      ]);

       $s = $request->input('input');
      $talents = Talent::latest()
          ->search($s)
          ->paginate(10);
      return view('talent-search', compact('talents','s'));

    }

}
