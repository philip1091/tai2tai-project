<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindUrlGenerator();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
        public function boot()
    {
        //
    }

    private function bindUrlGenerator()
    {
        $resolver = function ($app) {
            $routes = $app['router']->getRoutes();

            $request_rebinder = function ($app, $request) {
                $app['url']->setRequest($request);
            };

            $url = new \App\Routing\UrlGenerator(
                $routes,
                $app->rebinding('request', $request_rebinder),
                $app['config']['app.asset_url']
            );

            // Next we will set a few service resolvers on the URL generator so it can
            // get the information it needs to function. This just provides some of
            // the convenience features to this URL generator like "signed" URLs.
            $url->setSessionResolver(function () {
                return $this->app['session'];
            });

            $url->setKeyResolver(function () {
                return $this->app->make('config')->get('app.key');
            });

            // If the route collection is "rebound", for example, when the routes stay
            // cached for the application, we will need to rebind the routes on the
            // URL generator instance so it has the latest version of the routes.
            $app->rebinding('routes', function ($app, $routes) {
                $app['url']->setRoutes($routes);
            });

            return $url;
        };

        $this->app->singleton('url', $resolver);
        $this->app->singleton(UrlGeneratorContract::class, $resolver);
    }
}
