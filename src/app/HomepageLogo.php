<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomepageLogo extends Model
{
    protected $guarded = [];
}
