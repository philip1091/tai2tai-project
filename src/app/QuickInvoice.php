<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickInvoice extends Model
{
    protected $guarded = [];

    public function talent() {
      return $this->belongsTo(Talent::class);
    }

    public function users() {
      return $this->belongsToMany(User::class);
    }
}
