<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalentSoftSkill extends Model
{
    protected $guarded = [];
    public function talents() {
      return $this->belongsToMany(Talent::class);
    }
}
