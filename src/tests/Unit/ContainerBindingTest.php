<?php

namespace Tests\Unit;

use Tests\TestCase;

class ContainerBindingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_passes_if_urlgenerator_is_custom()
    {
        $urlgen = app(\Illuminate\Contracts\Routing\UrlGenerator::class);
        $this->assertInstanceOf(\App\Routing\UrlGenerator::class, $urlgen);
    }
}
