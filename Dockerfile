FROM industrydigital/php-nginx:1.2.0

ARG GIT_REVISION
ARG PREFIX=/var/www/html
ENV GIT_REVISION=$GIT_REVISION

COPY ./src /var/www/html
COPY ./docker/assets/nginx.conf /etc/nginx/nginx.conf

USER root
RUN set -xeu \
    && mkdir -p $PREFIX/storage/app/public \
    && mkdir -p $PREFIX/storage/logs \
    && mkdir -p $PREFIX/storage/temp \
    && mkdir -p $PREFIX/storage/framework/sessions \
    && mkdir -p $PREFIX/storage/framework/views \
    && mkdir -p $PREFIX/storage/framework/cache \
    && touch $PREFIX/storage/logs/laravel.log \
    && cd $PREFIX \
    && find . -type d -exec chmod 750 {} \; \
    && find . -type f -exec chmod 640 {} \; \
    && chown -R root:www-data $PREFIX \
    && chown -R www-data:www-data $PREFIX/storage \
    && mkdir -p /var/www/.config/psysh \
    && chown -R www-data:www-data /var/www/.config/psysh \
    && chmod 750 /var/www/.config/psysh

WORKDIR "/var/www/html"
VOLUME ["$PREFIX/storage", "/var/www/.config/psysh"]

