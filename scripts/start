#!/usr/bin/env bash
set -aeuo pipefail

if [ -n "$(git remote)" ]; then
    if [ ! $(which git-lfs) ]; then
        echo "You need Git LFS to run this project"
        echo ""
        echo "    https://github.com/git-lfs/git-lfs/wiki/Installation"
        echo ""
        exit 1
    fi
    git lfs pull
fi

git submodule init
git submodule update

# create the dotenv file if it doesn't exist
if [ ! -f .env ]; then
    source .env.default
    cp .env.default .env
    echo "DOCKER_HOST_UID=$(id -u)" >> .env
    echo "DOCKER_HOST_GID=$(id -g)" >> .env
    echo "COMPOSE_PROJECT_NAME=${PROJECT}" >> .env
    echo "Created boilerplate .env file at:"
    echo ""
    echo "    $(pwd)/.env"
    echo ""
fi

source .env.default
source .env

if [ -z "$APP_KEY" ]; then
    APP_KEY="base64:LsVBOXYlaRV81n00AbqKuWUnLou9yDWUeek+pTGW94U="
fi

# wait for mysql server to start accepting queries
init_mysql () {
    docker-compose exec --user=root db bash -c "chown -R mysql: /var/log/mysql"

    while [ 0 ]; do
        cmd="mysql -u root -p${DB_PASSWORD} -e 'SELECT \"READY\"' 2> /dev/null"
        result=$(docker-compose exec -T --user=root db bash -c "$cmd" || true)
        sleep 1
        if [[ "$result" =~ "READY" ]]; then
            break
        fi
    done

    docker-compose exec --user=root db bash -c "mysql -u root -p${DB_PASSWORD} -e \"SET GLOBAL general_log = 1\" 2> /dev/null";
    docker-compose exec --user=root db bash -c "mysql -u root -p${DB_PASSWORD} -e \"SET GLOBAL general_log_file = '/var/log/mysql/mysql.log'\" 2> /dev/null";
}

################################################################################

scripts/clean

echo "Launching containers"
docker-compose up -d --build

echo "Installing Composer dependencies"
scripts/composer install --ignore-platform-reqs

echo "Running database migrations and seeders..."
scripts/artisan migrate
scripts/artisan db:seed

echo "Laravel server listening on 0.0.0.0:$DOCKER_HOST_HTTP_BIND_PORT"
scripts/watch
